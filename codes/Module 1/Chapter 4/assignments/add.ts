var t1 : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var t2 : HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var ans : HTMLInputElement = <HTMLInputElement>document.getElementById("ans");

function add()
{
    var c : number = parseFloat(t1.value) + parseFloat(t2.value);
    ans.value = c.toString();
}
function sub()
{
    var c : number = parseFloat(t1.value) - parseFloat(t2.value);
    ans.value = c.toString();
}
function mul()
{
    var c : number = parseFloat(t1.value) * parseFloat(t2.value);
    ans.value = c.toString();
}
function div()
{
    if(parseFloat(t2.value)==0)
    {
        alert("Cannot Divide by 0");
    }
    else
    {
        var c : number = parseFloat(t1.value) / parseFloat(t2.value);
        ans.value = c.toString();
    }
}