var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var ans = document.getElementById("ans");
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    ans.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    ans.value = c.toString();
}
function mul() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    ans.value = c.toString();
}
function div() {
    if (parseFloat(t2.value) == 0) {
        alert("Cannot Divide by 0");
    }
    else {
        var c = parseFloat(t1.value) / parseFloat(t2.value);
        ans.value = c.toString();
    }
}
//# sourceMappingURL=add.js.map