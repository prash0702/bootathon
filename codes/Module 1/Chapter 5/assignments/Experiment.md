# PARALLELOGRAM LAW OF FORCES
> - ## Aim
> To verify the Parallelogram Law of Forces.

> - ## Apparatus
>> - Drawing sheet
>> - Pins
>> - Mirrors
>> - Weight pan
>> - Weights
>> - Pulleys

> - ## Theory
> The parallelogram law of forces enables us to determine the single force called resultant which can replace the two forces acting at a point with the same effect as that of the two forces. This law was formulated based on experimental results. This law states that if two forces acting simultaneously on a body at a point are represented in magnitude and direction by the two adjacent sides of a parallelogram, their resultant is represented in magnitude and direction by the diagonal of the parallelogram which passes through the point of intersection of the two sides representing the forces.
> $$ R = \sqrt{P^2} + {Q^2} + 2Pcos* $$
> Where R is the Resultant force and * is angle between P and Q.

> - ## Procedure
> 1. Two pulleys are mounted on a board, which is fixed on a wall. A string is passed through both the pulleys. Another string is tied to this string at the center of the pulleys. Weight pans are attached to the two ends of the string.
> 1. Fix a drawing sheet on the board. Add weights in any two pans in such a way that the sum of the weights in any two pans is greater than the third. The system of forces will come to equilibrium on some part of the drawing sheet.
> 1. Take a mirror and place it behind the strings. Mark the images of the strings on the drawing sheet. Remove the drawing sheet and find the resultant of the two forces graphically and analytically.
> 1. Change the weights and repeat the procedure for each set of weights.

> - ## Observation
|P|Q|R|Resultant R Graphically|Angle between P and Q|Resultant angle &alpha; between P and R|Angle &alpha; Analytically between P and R|Resultant R Analytically|
|----|----|----|----|----|----|----|----|
|150|150|150|2.95 x 50 = 147.5|119&deg;|60&deg;|59.5&deg;|152.25|